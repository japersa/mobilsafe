import React from 'react';

import {
  StyleSheet,
  View,
  ActivityIndicator,
  Modal,
  Dimensions,
} from 'react-native';

const height = Dimensions.get('screen').height;
const width = Dimensions.get('screen').width;

const Loader = ({
  visible,
  isModal,
  barHeight,
  color,
  hudColor,
  isHUD,
  size,
}) => {
  const renderWithModal = () => {
    return (
      <Modal
        transparent={true}
        animationType={'fade'}
        visible={visible}
        onRequestClose={() => {
          console.log('close modal');
        }}>
        <View style={[styles.modalBackground]}>
          <View
            style={[
              styles.activityIndicatorWrapper,
              {width: size, height: size},
              {
                backgroundColor: isHUD ? hudColor : 'transparent',
              },
            ]}>
            {renderActivityIndicator()}
          </View>
        </View>
      </Modal>
    );
  };
  const renderActivityIndicator = () => {
    const loaderColor = color;
    return isModal ? (
      <ActivityIndicator
        size="small"
        color={loaderColor}
        style={{zIndex: 100}}
        animating={visible}
      />
    ) : (
      <ActivityIndicator
        size="small"
        color={loaderColor}
        style={{zIndex: 100, marginBottom: barHeight}}
        animating={visible}
      />
    );
  };
  const renderWithView = () => {
    return (
      <View
        style={{
          height: height - barHeight,
          width: width,
          position: 'absolute',
          zIndex: 5,
          justifyContent: 'center',
          alignItems: 'center',
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.3)',
        }}>
        {renderActivityIndicator()}
      </View>
    );
  };

  const goEmpty = () => {
    return <View />;
  };

  return isModal ? renderWithModal() : visible ? renderWithView() : goEmpty();
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',
  },
  activityIndicatorWrapper: {
    backgroundColor: 'transparent',
    height: 60,
    width: 60,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});

export default Loader;
