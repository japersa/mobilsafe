/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Text,
  Platform,
  ScrollView,
  Alert,
  TextInput,
} from 'react-native';
import * as BlinkIDReactNative from 'blinkid-react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ImagePicker from 'react-native-image-picker';
import Loader from './Loader';

const Logo = require('./assets/logo.jpeg');

const licenseKey = Platform.select({
  // iOS license key for applicationID: org.reactjs.native.example.BlinkIDReactNative
  ios:
    'sRwAAAEtb3JnLnJlYWN0anMubmF0aXZlLmV4YW1wbGUuQmxpbmtJRFJlYWN0TmF0aXZlt67qu61k2vMma/ITqOzTsTu3cFDzoMfyAXiAVZRlx/y+9aYdWWV+PdhcUxEdmy9662wAwSK6Klc5R1iWaexp5IkOsBN/sf/JY6BeVnI382QKY7yPQGdRtXe1ZKvT0FSF/uELGKFMujoTbwzNuUPuLG60WXRB6aFzr1HmldpPqvfGVoiK7vg46pZa9f3yDPYlEJjKLdQCRBDKwNLSHNX/rF78gkK9HSqB/E1nn50z3aPnD5Gp29MSedzVahaa3vu8K4xm761zj0zkHwmRz4k8',
  // android license key for applicationID: com.blinkidreactnative
  android:
    'sRwAAAAWY29tLmJsaW5raWRyZWFjdG5hdGl2ZYouOuuUS2CbdVuoF2j8YQK5rx0ETrC9DHtMqZj5L052WrsLNOnxXLMhuFaLE6NMWgCaf9AXAlRYcZRQAbB3WKgMkJZjCb878gJ3CaseC0FSUq17n7Oumrtk4R8k9ll08vJ/ZPQtYFTMQaND2h4ryVpWY9ZDVeEXqLsF35rrdEuOtUrnqKVYyuQMK5MmU0B5jOlCcAfn7/ehaoFiMqdzoycGMash3x7utlbnlksr69d+1VU6kHdYVNnXQCw/yQEcfdskcRQ9iKCu0VBXl8IVSA==',
});

var renderIf = function (condition: boolean, content: JSX.Element) {
  if (condition) {
    return content;
  }
  return null;
};

const HomeScreen = () => {
  const [showFrontImageDocument, setShowFrontImageDocument] = useState(false);
  const [resultFrontImageDocument, setResultFrontImageDocument] = useState('');
  const [showBackImageDocument, setShowBackImageDocument] = useState(false);
  const [resultBackImageDocument, setResultBackImageDocument] = useState('');
  const [showImageFace, setShowImageFace] = useState(false);
  const [resultImageFace, setResultImageFace] = useState('');
  const [showSuccessFrame, setShowSuccessFrame] = useState(false);
  const [successFrame, setSuccessFrame] = useState('');
  const [results, setResults] = useState('');
  const [selfie, setSelfie] = useState(null);
  const [dniFullName, setDniFullName] = useState('');
  const [urlImageFace, setUrlImageFace] = useState('');
  const [urlImageSelfie, setUrlImageSelfie] = useState('');
  const [showLoading, setShowLoading] = useState(false);
  const [retry, setRetry] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [documentNumber, setDocumentNumber] = useState('');
  const [sex, setSex] = useState('');
  const [dateOfBirth, setDateOfBirth] = useState('');
  const [successDocumentValidation, setSuccessDocumentValidation] = useState(
    false,
  );

  const uploadImageFace = (base64image: string) => {
    try {
      setShowLoading(true);
      const body = JSON.stringify({
        base64image: base64image,
      });

      fetch('http://23.101.142.3:8083/upload', {
        method: 'POST',
        body: body,
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setUrlImageFace(responseJson.url);
          setShowLoading(false);
          createPersonGroup();
        })
        .catch((error) => {
          setShowLoading(false);
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  const uploadImage = (response) => {
    try {
      setRetry(false);
      setShowLoading(true);
      let formData = new FormData();
      formData.append('image', {
        name: response.fileName,
        type: response.type,
        uri:
          Platform.OS === 'android'
            ? response.uri
            : response.uri.replace('file://', ''),
      });

      fetch('http://23.101.142.3:8083/upload/image', {
        method: 'POST',
        body: formData,
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setUrlImageSelfie(responseJson.url);
          setShowLoading(false);
        })
        .catch(() => {
          setShowLoading(false);
          setRetry(true);
        });
    } catch (error) {
      setShowLoading(false);
      setRetry(true);
    }
  };

  const createPersonGroup = () => {
    try {
      setShowLoading(true);
      const group_id = 'mobilsoft';

      const body = JSON.stringify({
        name: 'mobilsoft',
        userData: 'user-provided data attached to the person group.',
      });

      fetch(
        `https://eastus.api.cognitive.microsoft.com/face/v1.0/persongroups/${group_id}`,
        {
          method: 'PUT',
          body: body,
          headers: {
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': '03a1cfc98af748bcad8dae4920237451',
          },
        },
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log('responseJson', responseJson);
          setShowLoading(false);
        })
        .catch((error) => {
          setShowLoading(false);
          showMessage('Error', 'Error al crear el grupo');
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  const createPersonInGroup = () => {
    try {
      setShowLoading(true);
      const group_id = 'mobilsoft';

      const body = JSON.stringify({
        name: dniFullName,
        userData: 'User-provided data attached to the person.',
      });

      fetch(
        `https://eastus.api.cognitive.microsoft.com/face/v1.0/persongroups/${group_id}/persons/`,
        {
          method: 'POST',
          body: body,
          headers: {
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': '03a1cfc98af748bcad8dae4920237451',
          },
        },
      )
        .then((response) => response.json())
        .then((responseJson) => {
          setShowLoading(false);
          addFaceToPerson(responseJson.personId);
        })
        .catch((error) => {
          setShowLoading(false);
          showMessage('Error', 'Error al crear a la persona');
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  const addFaceToPerson = (personId: string) => {
    try {
      setShowLoading(true);
      const group_id = 'mobilsoft';

      const body = JSON.stringify({
        url: urlImageSelfie,
      });

      fetch(
        `https://eastus.api.cognitive.microsoft.com/face/v1.0/persongroups/${group_id}/persons/${personId}/persistedFaces`,
        {
          method: 'POST',
          body: body,
          headers: {
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': '03a1cfc98af748bcad8dae4920237451',
          },
        },
      )
        .then((response) => response.json())
        .then((responseJson) => {
          setShowLoading(false);
          console.log('responseJson', responseJson);
          trainPersonGroup();
        })
        .catch((error) => {
          setShowLoading(false);
          showMessage('Error', 'Error al agregar rostro');
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  const trainPersonGroup = () => {
    try {
      setShowLoading(true);
      const group_id = 'mobilsoft';
      fetch(
        `https://eastus.api.cognitive.microsoft.com/face/v1.0/persongroups/${group_id}/train`,
        {
          method: 'POST',
          body: {},
          headers: {
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': '03a1cfc98af748bcad8dae4920237451',
          },
        },
      )
        .then((responseJson) => {
          setShowLoading(false);
          console.log('responseJson', responseJson);
          detectFace();
        })
        .catch((error) => {
          setShowLoading(false);
          showMessage('Error', 'Error al validar grupo');
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  const detectFace = () => {
    try {
      setShowLoading(true);
      const body = JSON.stringify({
        url: urlImageFace,
      });

      fetch('https://eastus.api.cognitive.microsoft.com/face/v1.0/detect', {
        method: 'POST',
        body: body,
        headers: {
          'Content-Type': 'application/json',
          'Ocp-Apim-Subscription-Key': '03a1cfc98af748bcad8dae4920237451',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setShowLoading(false);
          if (Array.isArray(responseJson) && responseJson.length === 0) {
            showMessage('Error', 'Tome una foto valida');
            setRetry(true);
          } else {
            identify(responseJson[0].faceId);
          }
        })
        .catch((error) => {
          setShowLoading(false);
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  const identify = (faceId: string) => {
    try {
      setShowLoading(true);
      const body = JSON.stringify({
        personGroupId: 'mobilsoft',
        faceIds: [faceId],
        confidenceThreshold: '.5',
      });

      fetch('https://eastus.api.cognitive.microsoft.com/face/v1.0/identify', {
        method: 'POST',
        body: body,
        headers: {
          'Content-Type': 'application/json',
          'Ocp-Apim-Subscription-Key': '03a1cfc98af748bcad8dae4920237451',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setShowLoading(false);
          if (Array.isArray(responseJson) && responseJson.length === 0) {
            showMessage('Error', 'La foto no coincide con la de la cedula');
            setRetry(true);
          } else {
            if (responseJson[0].candidates.length === 0) {
              showMessage('Error', 'La foto no coincide con la de la cedula');
              setRetry(true);
            } else {
              getName(responseJson[0].candidates[0].personId);
            }
          }
        })
        .catch((error) => {
          setShowLoading(false);
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  const getName = (faceId: string) => {
    try {
      setShowLoading(true);
      fetch(
        `https://eastus.api.cognitive.microsoft.com/face/v1.0/persongroups/mobilsoft/persons/${faceId}`,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': '03a1cfc98af748bcad8dae4920237451',
          },
        },
      )
        .then((response) => response.json())
        .then(() => {
          setShowLoading(false);
          setSuccessDocumentValidation(true);
          // showMessage(
          //   'Validacion exitosa',
          //   'La identificación ha sido validada exitosamente',
          // );
        })
        .catch((error) => {
          setShowLoading(false);
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  const scan = async () => {
    try {
      var blinkIdCombinedRecognizer = new BlinkIDReactNative.BlinkIdCombinedRecognizer();
      blinkIdCombinedRecognizer.returnFullDocumentImage = true;
      blinkIdCombinedRecognizer.returnFaceImage = true;

      const scanningResults = await BlinkIDReactNative.BlinkID.scanWithCamera(
        new BlinkIDReactNative.BlinkIdOverlaySettings(),
        new BlinkIDReactNative.RecognizerCollection([
          blinkIdCombinedRecognizer,
        ]),
        licenseKey,
      );

      if (scanningResults) {
        let newState = {
          showFrontImageDocument: false,
          resultFrontImageDocument: '',
          showBackImageDocument: false,
          resultBackImageDocument: '',
          showImageFace: false,
          resultImageFace: '',
          results: '',
          showSuccessFrame: false,
          successFrame: '',
        };

        for (let i = 0; i < scanningResults.length; ++i) {
          let localState = handleResult(scanningResults[i]);
          newState.showFrontImageDocument =
            newState.showFrontImageDocument ||
            localState.showFrontImageDocument;
          if (localState.showFrontImageDocument) {
            newState.resultFrontImageDocument =
              localState.resultFrontImageDocument;
          }
          newState.showBackImageDocument =
            newState.showBackImageDocument || localState.showBackImageDocument;
          if (localState.showBackImageDocument) {
            newState.resultBackImageDocument =
              localState.resultBackImageDocument;
          }
          newState.showImageFace =
            newState.showImageFace || localState.showImageFace;
          if (localState.resultImageFace) {
            newState.resultImageFace = localState.resultImageFace;
          }
          newState.results += localState.results;
          newState.showSuccessFrame =
            newState.showSuccessFrame || localState.showSuccessFrame;
          if (localState.successFrame) {
            newState.successFrame = localState.successFrame;
          }
        }
        newState.results += '\n';

        setShowFrontImageDocument(newState.showFrontImageDocument);
        setResultFrontImageDocument(newState.resultFrontImageDocument);
        setShowBackImageDocument(newState.showBackImageDocument);
        setResultBackImageDocument(newState.resultBackImageDocument);
        setShowImageFace(newState.showImageFace);
        setResultImageFace(newState.resultImageFace);
        setShowSuccessFrame(newState.showSuccessFrame);
        setSuccessFrame(newState.successFrame);
        setResults(newState.results);
      }
    } catch (error) {
      console.log(error);
      setShowFrontImageDocument(false);
      setResultFrontImageDocument('');
      setShowBackImageDocument(false);
      setResultBackImageDocument('');
      setShowImageFace(false);
      setResultImageFace('');
      setShowSuccessFrame(false);
      setSuccessFrame('');
      setResults('Scanning has been cancelled');
    }
  };

  const launchCamera = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        setSelfie(response.data);
        uploadImage(response);
      }
    });
  };

  const handleResult = (result: any) => {
    let fieldDelim = '\n';

    var localState = {
      showFrontImageDocument: false,
      resultFrontImageDocument: '',
      showBackImageDocument: false,
      resultBackImageDocument: '',
      resultImageFace: '',
      results: '',
      showSuccessFrame: false,
      successFrame: '',
      showImageFace: false,
    };

    if (result instanceof BlinkIDReactNative.BlinkIdCombinedRecognizerResult) {
      let blinkIdResult = result;

      setDniFullName(blinkIdResult.firstName + ' ' + blinkIdResult.lastName);

      setFirstName(blinkIdResult.firstName);
      setLastName(blinkIdResult.lastName);
      setDocumentNumber(blinkIdResult.documentNumber);
      setSex(blinkIdResult.sex);

      let resultString =
        'Nombres: ' +
        blinkIdResult.firstName +
        fieldDelim +
        'Apellidos: ' +
        blinkIdResult.lastName +
        fieldDelim +
        'Numero de documento: ' +
        blinkIdResult.documentNumber +
        fieldDelim +
        'Sexo: ' +
        blinkIdResult.sex +
        fieldDelim;
      if (blinkIdResult.dateOfBirth) {
        resultString +=
          'Fecha de nacimiento ' +
          blinkIdResult.dateOfBirth.day +
          '.' +
          blinkIdResult.dateOfBirth.month +
          '.' +
          blinkIdResult.dateOfBirth.year +
          '.' +
          fieldDelim;
        setDateOfBirth(
          blinkIdResult.dateOfBirth.day +
            '.' +
            blinkIdResult.dateOfBirth.month +
            '.' +
            blinkIdResult.dateOfBirth.year +
            '.',
        );
      }

      localState.results += resultString;

      if (blinkIdResult.fullDocumentFrontImage) {
        localState.showFrontImageDocument = true;
        localState.resultFrontImageDocument =
          'data:image/jpg;base64,' + blinkIdResult.fullDocumentFrontImage;
      }
      if (blinkIdResult.fullDocumentBackImage) {
        localState.showBackImageDocument = true;
        localState.resultBackImageDocument =
          'data:image/jpg;base64,' + blinkIdResult.fullDocumentBackImage;
      }
      // Face image is returned as Base64 encoded JPEG
      if (blinkIdResult.faceImage) {
        localState.showImageFace = true;
        localState.resultImageFace =
          'data:image/jpg;base64,' + blinkIdResult.faceImage;
        uploadImageFace(blinkIdResult.faceImage);
      }
    }
    return localState;
  };

  const showMessage = (title: string, message: string) => {
    setTimeout(() => {
      Alert.alert(title, message);
    }, 200);
  };

  return (
    <>
      <Loader
        visible={showLoading}
        isModal={true}
        isHUD={true}
        color={'#FFFFFF'}
        hudColor={'#000000'}
        barHeight={64}
        size={60}
      />
      <SafeAreaView />
      <View style={styles.container}>
        {results === '' ? (
          <>
            <View style={styles.containerLogin}>
              <View style={styles.containerLogo}>
                <Image style={styles.logo} source={Logo} />
                <Text style={styles.title}>VAMOS A VERIFICAR TU IDENTIDAD</Text>
                <Text style={styles.description}>
                  Por seguridad vamos a comparar la foto en tu documento contra
                  una foto recien tomada
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginTop: 16,
                }}>
                <Icon name="eye" color={'grey'} size={24} />
                <Text style={[styles.description, {marginLeft: 5}]}>
                  Retira tus gafas, para ver tus ojos
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginTop: 16,
                  marginBottom: 16,
                }}>
                <Icon name="face-recognition" color={'grey'} size={24} />
                <Text style={[styles.description, {marginLeft: 5}]}>
                  Manten tu cara seria
                </Text>
              </View>
              <TouchableOpacity
                style={styles.buttonRegister}
                onPress={() => {
                  scan();
                }}>
                <Text style={styles.textButtonRegister}>VAMOS A HACERLO</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex: 1, backgroundColor: '#f7f7f7'}} />
            <View style={{margin: 16, backgroundColor: '#f7f7f7'}}>
              <Text style={styles.powered}>
                Powered by Mobilsoft SAS, Barranquilla, Colombia
              </Text>
              <Text style={styles.powered}>
                Whatsapp : 3008518215 - gerencia@mobilsoft.co
              </Text>
              <Text style={styles.powered}>www.mobilsoft.co</Text>
            </View>
          </>
        ) : successDocumentValidation ? (
          <>
            <View style={styles.containerLogin}>
              <View
                style={{
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginTop: 150,
                  marginBottom: 32,
                }}>
                <Icon name="face-recognition" color={'grey'} size={72} />
                <Text style={[styles.description, {marginTop: 32}]}>
                  Tu identidad ha sido verificada exitosamente
                </Text>
              </View>
              <TouchableOpacity
                style={styles.buttonRegister}
                onPress={() => {
                  setShowFrontImageDocument(false);
                  setResultFrontImageDocument('');
                  setShowBackImageDocument(false);
                  setResultBackImageDocument('');
                  setShowImageFace(false);
                  setResultImageFace('');
                  setShowSuccessFrame(false);
                  setSuccessFrame('');
                  setResults('');
                  setSelfie(null);
                  setDniFullName('');
                  setUrlImageFace('');
                  setUrlImageSelfie('');
                  setDniFullName('');
                  setFirstName('');
                  setLastName('');
                  setDocumentNumber('');
                  setSex('');
                  setDateOfBirth('');
                  setSuccessDocumentValidation(false);
                }}>
                <Text style={styles.textButtonRegister}>VOLVER A HACERLO</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex: 1, backgroundColor: '#f7f7f7'}} />
            <View style={{margin: 16, backgroundColor: '#f7f7f7'}}>
              <Text style={styles.powered}>
                Powered by Mobilsoft SAS, Barranquilla, Colombia
              </Text>
              <Text style={styles.powered}>
                Whatsapp : 3008518215 - gerencia@mobilsoft.co
              </Text>
              <Text style={styles.powered}>www.mobilsoft.co</Text>
            </View>
          </>
        ) : (
          <>
            <View style={styles.header}>
              <TouchableOpacity
                style={styles.buttonBack}
                onPress={() => {
                  setShowFrontImageDocument(false);
                  setResultFrontImageDocument('');
                  setShowBackImageDocument(false);
                  setResultBackImageDocument('');
                  setShowImageFace(false);
                  setResultImageFace('');
                  setShowSuccessFrame(false);
                  setSuccessFrame('');
                  setResults('');
                  setSelfie(null);
                  setDniFullName('');
                  setUrlImageFace('');
                  setUrlImageSelfie('');
                  setDniFullName('');
                  setFirstName('');
                  setLastName('');
                  setDocumentNumber('');
                  setSex('');
                  setDateOfBirth('');
                }}>
                <Icon name="arrow-left" color={'grey'} size={24} />
              </TouchableOpacity>
            </View>
            <ScrollView>
              <View style={[styles.card, {marginTop: 10}]}>
                <View
                  style={[
                    styles.containerTitleCard,
                    {
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    },
                  ]}>
                  <Text style={styles.cardTitle}>DATOS DOCUMENTO</Text>
                </View>
                <View
                  style={{
                    margin: 16,
                  }}>
                  <View style={{flex: 1}}>
                    <View style={styles.inputContainer}>
                      <Text style={styles.textInput}>Nombres</Text>
                      <TextInput
                        style={[styles.input]}
                        value={firstName}
                        placeholder={''}
                        editable={false}
                        placeholderTextColor={'#3f3f3f'}
                      />
                    </View>
                    <View style={styles.inputContainer}>
                      <Text style={styles.textInput}>Apellidos</Text>
                      <TextInput
                        style={[styles.input]}
                        value={lastName}
                        placeholder={''}
                        editable={false}
                        placeholderTextColor={'#3f3f3f'}
                      />
                    </View>
                    <View style={styles.inputContainer}>
                      <Text style={styles.textInput}>Numero de documento</Text>
                      <TextInput
                        style={[styles.input]}
                        value={documentNumber}
                        placeholder={''}
                        editable={false}
                        placeholderTextColor={'#3f3f3f'}
                      />
                    </View>
                    <View style={styles.inputContainer}>
                      <Text style={styles.textInput}>Sexo</Text>
                      <TextInput
                        style={[styles.input]}
                        value={sex}
                        placeholder={''}
                        editable={false}
                        placeholderTextColor={'#3f3f3f'}
                      />
                    </View>
                    <View style={styles.inputContainer}>
                      <Text style={styles.textInput}>Fecha de nacimiento</Text>
                      <TextInput
                        style={[styles.input]}
                        value={dateOfBirth}
                        placeholder={''}
                        editable={false}
                        placeholderTextColor={'#3f3f3f'}
                      />
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.card}>
                <View
                  style={[
                    styles.containerTitleCard,
                    {
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    },
                  ]}>
                  <Text style={styles.cardTitle}>FOTO DOCUMENTO</Text>
                </View>
                <View style={styles.form}>
                  {renderIf(
                    showImageFace,
                    <View style={styles.imageContainer}>
                      <Image
                        resizeMode="contain"
                        source={{uri: resultImageFace, scale: 3}}
                        style={styles.imageResult}
                      />
                    </View>,
                  )}
                </View>
              </View>
              <View style={styles.card}>
                <View
                  style={[
                    styles.containerTitleCard,
                    {
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    },
                  ]}>
                  <Text style={styles.cardTitle}>CARA FRONTAL DOCUMENTO</Text>
                </View>

                <View style={styles.form}>
                  {renderIf(
                    showFrontImageDocument,
                    <View style={styles.imageContainer}>
                      <Image
                        resizeMode="contain"
                        source={{uri: resultFrontImageDocument, scale: 3}}
                        style={styles.imageResult}
                      />
                    </View>,
                  )}
                </View>
              </View>

              <View style={styles.card}>
                <View
                  style={[
                    styles.containerTitleCard,
                    {
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    },
                  ]}>
                  <Text style={styles.cardTitle}>CARA REVERSO DOCUMENTO</Text>
                </View>
                <View style={styles.form}>
                  {renderIf(
                    showBackImageDocument,
                    <View style={styles.imageContainer}>
                      <Image
                        resizeMode="contain"
                        source={{uri: resultBackImageDocument, scale: 3}}
                        style={styles.imageResult}
                      />
                    </View>,
                  )}
                </View>
              </View>
              {selfie != null && (
                <View style={styles.card}>
                  <View
                    style={[
                      styles.containerTitleCard,
                      {
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      },
                    ]}>
                    <Text style={styles.cardTitle}>SELFIE</Text>
                  </View>
                  <View style={styles.form}>
                    <Image
                      resizeMode="cover"
                      style={{height: 200, width: 200, alignSelf: 'center'}}
                      source={{
                        uri: `data:image/jpg;base64,${selfie}`,
                      }}
                    />
                  </View>
                </View>
              )}
            </ScrollView>
            <View style={{flex: 1, backgroundColor: '#f7f7f7'}} />
            <View style={{margin: 16, backgroundColor: '#f7f7f7'}}>
              {selfie != null && !retry ? (
                <>
                  <TouchableOpacity
                    style={styles.buttonRegister}
                    onPress={() => {
                      createPersonInGroup();
                    }}>
                    <Text style={styles.textButtonRegister}>
                      VALIDAR IDENTIDAD
                    </Text>
                  </TouchableOpacity>
                </>
              ) : (
                <>
                  <TouchableOpacity
                    style={styles.buttonRegister}
                    onPress={() => {
                      launchCamera();
                    }}>
                    <Text style={styles.textButtonRegister}>TOMAR SELFIE</Text>
                  </TouchableOpacity>
                </>
              )}
            </View>
          </>
        )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f7f7',
  },
  containerLogin: {
    margin: 16,
  },
  sectionStyle: {
    paddingLeft: 8,
    paddingRight: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#226fb8',
    height: 40,
    borderRadius: 5,
    marginBottom: 16,
  },
  form: {
    padding: 16,
  },
  containerLogo: {
    backgroundColor: '#f7f7f7',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 150,
    height: 150,
    margin: 16,
  },
  button: {
    justifyContent: 'center',
    marginTop: 30,
    height: 44,
    backgroundColor: '#226fb8',
    borderRadius: 5,
  },
  textButton: {
    textTransform: 'uppercase',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  title: {
    marginTop: 16,
    textTransform: 'uppercase',
    fontSize: 18,
    color: '#226fb8',
    padding: 16,
    textAlign: 'center',
  },
  description: {
    fontSize: 15,
    color: '#4f4f4f',
    textAlign: 'center',
  },
  label: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 50,
  },
  buttonContainer: {
    margin: 20,
  },
  imageContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  results: {
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#4f4f4f',
    letterSpacing: 0.32,
  },
  imageResult: {
    flex: 1,
    flexShrink: 1,
    height: 200,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    borderRadius: 10,
  },
  card: {
    backgroundColor: '#f7f7f7',
  },
  containerTitleCard: {
    borderBottomWidth: 1,
    borderBottomColor: '#0000000d',
  },
  containerBottomCard: {
    borderTopWidth: 1,
    borderTopColor: '#0000000d',
  },
  cardTitle: {
    paddingRight: 16,
    paddingLeft: 16,
    paddingBottom: 8,
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#4f4f4f',
    fontWeight: '600',
    letterSpacing: 0.32,
  },
  buttonRegister: {
    justifyContent: 'center',
    marginTop: 20,
    height: 44,
    backgroundColor: '#1a5584',
    borderRadius: 5,
  },
  textButtonRegister: {
    textTransform: 'uppercase',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  signature: {
    borderColor: '#000033',
    borderWidth: 1,
  },
  powered: {
    color: '#8A8A8A',
    marginBottom: 10,
    fontSize: 11,
    textAlign: 'center',
  },
  textInput: {
    color: '#8A8A8A',
    marginBottom: 10,
    fontSize: 11,
  },
  inputContainer: {
    marginBottom: 16,
  },
  input: {
    height: 44,
    color: '#3F3F3F',
    backgroundColor: '#ededed',
    borderRadius: 4,
    paddingStart: 10,
    paddingEnd: 10,
  },
  header: {
    height: 44,
    backgroundColor: '#f7f7f7',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#0000000d',
  },
  buttonBack: {
    marginLeft: 16,
  },
});

export default HomeScreen;
